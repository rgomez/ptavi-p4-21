#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
try:
    SERVER = 'localhost'
    ADIOS = 'Adiós'
    IP = str(sys.argv[1])
    PORT = int(sys.argv[2])
    METHOD = str(sys.argv[3])
    USER = str(sys.argv[4])
    EXPIRES = int(sys.argv[5])
    REQUEST = METHOD + " " + "sip: " + USER + " " + "SIP/2.0\r\n\r\n" + " " + str(EXPIRES)
except IndexError:
    print("Usage: client.py ip puerto register sip_address expires_value")


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))

            print(f"Enviando a {SERVER}:{PORT}:", REQUEST)
            my_socket.send(REQUEST.encode('utf-8') + b'\r\n')
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))
            print(f"Enviando a {SERVER}:{PORT}:", ADIOS)
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()

#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time

# Constantes. Puerto.
PORT = 5060


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        IP = self.client_address[0]
        self.wfile.write("Hemos recibido tu petición".encode('utf-8'))
        readline = self.rfile.read()
        method = readline.decode('utf-8').split(" ")[0]
        user = readline.decode('utf-8').split(" ")[2]
        expires = readline.decode('utf-8').split(" ")[4]
        expiretime = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time())) + "+" + expires

        if method == "REGISTER" or "register" and int(expires) != 0:
            print("SIP/2.0" + " " + "200" + " " + "OK\r\n\r\n")
            for line in self.rfile:
                print("SIP/2.0" + " " + "200" + " " + "OK\r\n\r\n", line.decode('utf-8'))
            dic = {'user': user, 'ip': IP}
            print(dic)
            if int(expires) == 0:
                print("SIP/2.0" + " " + "200" + " " + "OK\r\n\r\n")
                for line in self.rfile:
                    print("SIP/2.0" + " " + "200" + " " + "OK\r\n\r\n", line.decode('utf-8'))
                del dic['user']
                del dic['ip']
                print(dic)

        def register2json():
            dic = {user: ['address: ' + IP, 'expire: ' + expiretime]}
            if int(expires) == 0:
                print("SIP/2.0" + " " + "200" + " " + "OK\r\n\r\n")
                for line in self.rfile:
                    print("SIP/2.0" + " " + "200" + " " + "OK\r\n\r\n", line.decode('utf-8'))
                del dic[user]
                print(dic)
            for line in self.rfile:
                print("SIP/2.0" + " " + "200" + " " + "OK\r\n\r\n", line.decode('utf-8'))

            with open('registered.json', 'w') as jsonfichero:
                json.dump(dic, jsonfichero, indent=2)

        def json2registered():
            with open('registered.json', 'r') as jsonfichero:
                dic = json.load(jsonfichero)

        register2json()
        json2registered()


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server listening in port ({PORT})")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
